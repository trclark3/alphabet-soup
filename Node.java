import java.util.ArrayList;


public class Node{
	ArrayList<Node> children;
	private String key;
	private boolean flag = false;
	public Node(String key){
		this.children = new ArrayList<Node>();
		this.key = key;
	}
	
	public void add(String word) {//this works
		if(word.length() > 0) {
			int hold = -1;
			loop: for(int i = 0; i < this.children.size(); i++) {
				if(word.substring(0,1).equalsIgnoreCase(this.children.get(i).getKey())){
					hold = i;
					break loop;
				}
			}
			if(hold != -1){
				if(word.length() > 1) {
					this.children.get(hold).add(word.substring(1));
				}
				else {
					this.children.get(hold).setFlag(true);
				}
			}
			else {
				this.children.add(new Node(word.substring(0,1)));
				if(word.length() > 1) {
					this.children.get(this.children.size()-1).add(word.substring(1));
				}
				else {
					this.children.get(this.children.size()-1).setFlag(true);
				}
			}
		}
		else {
			this.flag = true;
		}
	}
	
	public int search(String word){
		if(this.flag == true) {
			return 0;
		}
		for(int i = 0; i < this.children.size(); i++) {
			if(word.length() > 0 && word.substring(0,1).equalsIgnoreCase(this.children.get(i).getKey())) {
				if(word.length() > 0) {
					return 1 + this.children.get(i).search(word.substring(1));
				}
				else {
					return 0;//may need to be 0 prob not but like IDK
				}
			}
		}
		return -9999999;
	}
	
	public String getKey() {
		return this.key;
	}
	
	public void setFlag(boolean t) {
		this.flag = t;
	}
	
	public boolean getFlag() {
		return this.flag;
	}
	
	public void print() {//this works
		System.out.print(this.key);	
		for(int i = 0; i < this.children.size(); i++) {
			this.children.get(i).print();
		}
	}
}
