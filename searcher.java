import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;




public class searcher {
	public static Node root = new Node("");
	public static void main(String[] args) throws FileNotFoundException {
		//load input
		Scanner scan = new Scanner(System.in);
		int dim[] = new int[2];
		String hold[] = scan.nextLine().split("x");
		dim[0] = Integer.parseInt(hold[0]);
		dim[1] = Integer.parseInt(hold[1]);//this works
		//set up board
		String board[][] = new String[dim[0]][dim[1]];
		for(int i = 0; i < dim[0]; i++) {
			board[i] = scan.nextLine().split(" ");//this works
		}
		String line = "";
		while(scan.hasNext()) {
			line += " " + scan.nextLine();
		}
		String words[] = line.strip().split(" ");
		int max = 0; 
		for(int i = 0; i < words.length; i ++){//this works
			if(words[i].length() > max) {
				max = words[i].length();
			}
			root.add(words[i]);
		}
		//end of set up System.out.println(search);
		//begin search phase System.out.print("good");
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[i].length; j++) {
				//basically in all 8 directions we want to grab the longest line of input
				String search = "";
				for(int k = 0; k < max; k++) {//down
					if(i+k<board.length) {
						search += board[i+k][j];
					}
				}
				int found = root.search(search);
				if(found > 0) {
					System.out.println(search.substring(0,found)+ " " + i + ":" + j + " " + (i + found-1) + ":" + j);
				}
				search = "";
				for(int k = 0; k < max; k++) {//up
					if(i-k>=0) {
						search += board[i-k][j];
					}
				}
				found = root.search(search);
				if(found > 0) {
					System.out.println(search.substring(0,found)+ " " + i + ":" + j + " " + (i-found+1) + ":" + j);
				}				
				search = "";
				for(int k = 0; k < max; k++) {//left
					if(j+k<board.length) {
						search += board[i][j+k];
					}
				}
				found = root.search(search);
				if(found > 0) {
					System.out.println(search.substring(0,found)+ " " + i + ":" + j + " " + i + ":" + (j+found-1));
				}
				search = "";
				for(int k = 0; k < max; k++) {//right
					if(j-k>=0) {
						search += board[i][j-k];
					}
				}
				found = root.search(search);
				if(found > 0) {
					System.out.println(search.substring(0,found)+ " " + i + ":" + j + " " + i + ":" + (j-found+1));
				}
				search = "";
				for(int k = 0; k < max; k++) {//top left
					if(j-k>=0 && i-k>=0) {
						search += board[i-k][j-k];
					}
				}
				found = root.search(search);
				if(found > 0) {
					System.out.println(search.substring(0,found)+ " " + i + ":" + j + " " + (i-found+1) + ":" + (j-found+1));
				}
				search = "";
				for(int k = 0; k < max; k++) {//top left
					if(j+k<board.length && i-k>=0) {
						search += board[i-k][j+k];
					}
				}
				found = root.search(search);
				if(found > 0) {
					System.out.println(search.substring(0,found)+ " " + i + ":" + j + " " + (i-found+1) + ":" + (j+found-1));
				}
				search = "";
				for(int k = 0; k < max; k++) {//top left
					if(j-k>=0 && i+k<board.length) {
						search += board[i+k][j-k];
					}
				}
				found = root.search(search);
				if(found > 0) {
					System.out.println(search.substring(0,found)+ " " + i + ":" + j + " " + (i+found-1) + ":" + (j-found+1));
				}
				search = "";
				for(int k = 0; k < max; k++) {//top left
					if(j+k<board.length && i+k<board.length) {
						search += board[i+k][j+k];
					}
				}
				found = root.search(search);
				if(found > 0) {
					System.out.println(search.substring(0,found)+ " " + i + ":" + j + " " + (i+found-1) + ":" + (j+found-1));
				}
			}
		}
	}
}
